<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [CastController::class, 'mainMenu']);

// CRUD
// C => Creata Data
// Mengarahkan ke form tambah cast
Route::get('/tambahCast', [CastController::class, 'create']); 
// Menyimpan data yang didapat dari form ke Database
Route::post('/cast', [CastController::class, 'store']);

// R => Read Data
// Menampilkan tabel Cast
Route::get('/cast', [CastController::class, 'index']);
// Detail Category berdasarkan ID (tombol detail pada kolom action)
Route::get('/cast/{id}', [CastController::class, 'show']);

// U => Update Data
// mengarahkan ke form edit cast berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// Update data berdasarkan parameter
Route::put('/cast/{id}', [CastController::class, 'update']);

// D => Detele Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);


// CRUD FILM
Route::resource('film', FilmController::class);

// CRUD GENRE
Route::resource('genre', GenreController::class);
