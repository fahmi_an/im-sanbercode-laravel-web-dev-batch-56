@extends('layouts.master')

@section('title')
Halaman Detail Cast
@endsection

@section('content')
<h1>{{$cast->name}}</h1>
<p>Umur : {{$cast->umur}}</p>
<p>Biodata : {{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection