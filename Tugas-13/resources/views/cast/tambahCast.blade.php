@extends('layouts.master')

@section('title')
Tambah Cast
@endsection

@section('content')
<form method="post" action="/cast">
  <!-- ini merupakan codingan validasi yang didapat dari dokumentasi laravelnya -->
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <!-- Form Input -->
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="name" class="form-control">
  </div>
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control">
  </div>
  <div class="form-group">
    <label>Biodata</label>
    <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection