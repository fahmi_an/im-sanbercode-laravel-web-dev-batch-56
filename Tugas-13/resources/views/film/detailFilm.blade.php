@extends('layouts.master')

@section('title')
Detail Film
@endsection

@section('content')

<img src="{{asset('poster/'.$film->poster)}}" width="50%" height="50%" alt="">

<h1 class="text-primary">{{$film->judul}}</h1>
<h3>Tahun Rilis : {{$film->tahun}}</h3>
<h3>Genre : {{$genre->nama}}</h3> <!-- menampilkan nama genre -->
<p>{{$film->ringkasan}}</p>

@endsection