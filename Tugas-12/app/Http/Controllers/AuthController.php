<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signUp() { //function yang dipanggil di coding web.php
        return view('page.form'); //view('..') akan menampilkan halaman welcome, page. untuk memasuki folder dimana file form berada.
    }

    public function welcome(Request $request) { //$request digunakan untuk mendapatkan inputan dari form
        // dd($request->input('fname')); // digunakan untuk mengetahui nilai dari inputan yang dimasukan

        // code dibawah ini digunakan untuk mengetahui dan menangkap nilai dari inputan yang diimasukkan
        $fname = $request->input('fname');
        $lname = $request->input('lname');
        $gender = $request->input('gender');
        $country = $request->input('country');
        $lang = $request->input('lang');
        $bio = $request->input('bio');

        return view('page.welcome', ['fname'=>$fname, 'lname'=>$lname, 'gender'=>$gender, 'country'=>$country, 'lang'=>$lang, 'bio'=>$bio]); //return view ini digunakan untuk menampilkan atau mengirim hasil inputan kepada halaman welcome.
    }
}
