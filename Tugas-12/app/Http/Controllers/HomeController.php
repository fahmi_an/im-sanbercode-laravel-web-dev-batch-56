<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function mainMenu() { //function yang dipanggil di coding web.php
        return view('index'); //view('..') akan menampilkan halaman welcome
    }
}
