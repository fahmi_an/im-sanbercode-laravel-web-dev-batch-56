<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function data1() { //function yang dipanggil di coding web.php
        return view('/tabel'); //view('..') akan menampilkan halaman welcome
    }

    public function data2() { //function yang dipanggil di coding web.php
        return view('/data-tabel'); //view('..') akan menampilkan halaman welcome
    }
}
