<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'mainMenu']); //route untuk menjalankan function main menu, dimana ini mengarah ke file welcome pada function tersebut.

Route::get('/form', [AuthController::class, 'signUp']);

Route::post('/welcome', [AuthController::class, 'welcome']); //jika method pada form menggunakan post, maka pada route juga harus menggunakan post bukan get.

Route::get('/master', function(){
    return view('layouts.master');
});

Route::get('/dataTabel', function() {
    return view('page.dataTabel');
});

Route::get('/tabel', function() {
    return view('page.tabel');
});
