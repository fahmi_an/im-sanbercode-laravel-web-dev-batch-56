<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Laman Form</title>
  </head>
  <body>
    <h1><label for="sup">Sign Up Form</label></h1>
    <!-- method post ini digunakan untuk mengirim apa yang kita masukkan kedalam form -->
    <form action="/welcome" method="POST">
      <!-- token csrf digunakan untuk mendapatkan csrf protection (proteksi dari hacker) -->
      @csrf 
      <label for="fname">First Name :</label> <br />
      <br />
      <input type="text" name="fname" /> <br />
      <br />

      <label for="lname">Last Name :</label> <br />
      <br />
      <input type="text" name="lname" /> <br />
      <br />

      <label for="gender">Gender :</label> <br />
      <br />
      <input type="radio" id="gender" value="1" name="gender" />Male <br />
      <input type="radio" id="gender" value="2" name="gender" />Female <br />
      <input type="radio" id="gender" value="3" name="gender" />Other <br />
      <br />

      <label for="country">Nationality :</label> <br />
      <br />
      <select name="country" id="country">
        <option value="1">Indonesia</option>
        <option value="2">England</option>
        <option value="3">France</option>
      </select>
      <br />
      <br />

      <label for="lang">Language : </label> <br />
      <br />
      <input type="checkbox" name="lang" />Indonesia <br />
      <input type="checkbox" name="lang" />Sunda <br />
      <input type="checkbox" name="lang" />Jawa <br />
      <br />

      <label for="bio">Bio :</label> <br />
      <textarea name="bio" cols="30" rows="10"></textarea> <br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>