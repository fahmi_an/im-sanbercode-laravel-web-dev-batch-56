<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP</title>
</head>
<body>
    <?php
        require_once('animal.php');
        require_once('ape.php');
        require_once('lion.php');

            $cat = new Animal("Garfield");
            echo "Nama hewan : " . $cat->name . "<br>";
            echo "Jumlah Kaki hewan : " . $cat->legs . "<br>";
            echo "Berdarah dingin : " . $cat->cold_blooded . "<br> <br>";

            $ape = new ape("Wukong");
            echo "Nama hewan : " . $ape->name . "<br>";
            echo "Jumlah Kaki hewan : " . $ape->legs . "<br>";
            echo "Berdarah dingin : " . $ape->cold_blooded ."<br>";
            echo "Suaranya : " . $ape->yell . "<br><br>";

            $lion = new lion("Simba");
            echo "Nama hewan : " . $lion->name . "<br>";
            echo "Jumlah Kaki hewan : " . $lion->legs . "<br>";
            echo "Berdarah dingin : " . $lion->cold_blooded ."<br>";
            echo "Suaranya : " . $lion->yell . "<br>";
            echo "Julukannya : " . $lion->julukan . "<br>";
    ?>
</body>
</html>