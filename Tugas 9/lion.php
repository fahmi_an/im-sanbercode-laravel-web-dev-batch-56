<?php
    require_once('animal.php');

    class lion extends Animal {
        public $cold_blooded = "Yes";
        public $yell = "Raaaawwr";
        public $julukan = "Raja Rimba";
    }
?>