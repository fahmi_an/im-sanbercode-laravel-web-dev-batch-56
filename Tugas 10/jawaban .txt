1. Buat Database :
CREATE DATABASE myshop;

2. Membuat Table
- USERS : CREATE TABLE users ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL, email varchar(255) NOT NULL, password varchar(255) NOT NULL );
- CATEGORIES : CREATE TABLE users ( id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL);
- ITEMS : CREATE TABLE items ( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, categories_id int, FOREIGN KEY (categories_id) REFERENCES categories(id) );

3. Memasukkan Data ke Table
- Table users :
INSERT INTO users (name, email, password) VALUES("Hendrik", "hendrik@emai.com", "hendrik123"),("Fahmi","fahmi@emai.com","fahmi123"),("Ujang","ujang@emai.com","ujang123");

- Table categories :
INSERT INTO categories (name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

- Table Items : 
INSERT INTO items (name, description, price, stock, categories_id) VALUES("Sumsang B50","hp keren dari sumsang","4000000","100","1"),("uniklooh","baju keren dari brand unik","500000","50","2"),("IMHO watch","jam tangan anak yang jujur banget","2000000","10","1");

4. Mengambil Data
a. table users : SELECT name, email FROM users;
b.1. mengambil data dengan Harga > 1000000 di table items : SELECT * FROM `items` WHERE price > 1000000;
b.2. menampilkan data terdapat kata SANG : SELECT * FROM items WHERE name LIKE "%sang%";
c. menggabungkan table : SELECT items.*, categories.name FROM items INNER JOIN categories ON items.categories_id = categories.id;

5. Mengubah data pada table item
UPDATE items SET price = 2500000 WHERE id = 1;
