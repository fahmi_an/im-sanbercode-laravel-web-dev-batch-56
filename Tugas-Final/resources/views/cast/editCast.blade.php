@extends('layouts.master')

@section('title')
Edit Cast
@endsection

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
  <!-- method digunakan untuk mengganti method POST pada form diatas menjadi put -->
  @method('put')
  <!-- ini merupakan codingan validasi yang didapat dari dokumentasi laravelnya -->
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <!-- Form Input -->
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$cast->name}}" name="name" class="form-control">
  </div>
  <div class="form-group">
    <label>Umur</label>
    <input type="text" value="{{$cast->umur}}" name="umur" class="form-control">
  </div>
  <div class="form-group">
    <label>Biodata</label>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection