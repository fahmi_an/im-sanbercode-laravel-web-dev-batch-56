<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $film = Film::get();

        return view ('film.tampilFilm', ['film' => $film]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $genre = Genre::get();

        return view('film.tambahFilm', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'poster' => 'required|mimes:jpeg,png,jpg|max:2048',
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required'
        ]);
      
        $posterName = time().'.'.$request->poster->extension(); 
       
        $request->poster->move(public_path('poster'), $posterName);

        $film = new Film;
 
        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->genre_id = $request->input('genre_id');
        $film->poster = $posterName;
 
        $film->save();
 
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $film = Film::find($id);
        $genre = Genre::find($film->genre_id); // Passing nama genre berdasarkan gere_id

        return view('film.detailFilm', ['film' => $film, 'genre' => $genre]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $film = Film::find($id);
        $genre = Genre::get();

        return view('film.editFilm', ['film' => $film, 'genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'poster' => 'required|mimes:jpeg,png,jpg|max:2048',
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required'
        ]);

        $film = Film::find($id);

        if($request->has('poster')) {

            // Menghapus gambar lama
            $path = 'poster/';
            File::delete($path . $film->poster);


            // Mengganti gambar baru
            $posterName = time().'.'.$request->poster->extension(); 
       
            $request->poster->move(public_path('poster'), $posterName);

            // Mengganti gambar di database
            $film->poster = $posterName;
        }

        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->genre_id = $request->input('genre_id');
 
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $film = Film::find($id);

            // Menghapus gambar lama
            $path = 'image/';
            File::delete($path . $film->poster);

            $film->delete();

        return redirect('/film');
    }
}
