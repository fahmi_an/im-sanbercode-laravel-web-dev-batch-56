<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = "film"; //Mengarahkan ke database film kita

    protected $fillable = ['id', 'judul', 'ringkasan', 'tahun', 'poster', 'genre_id']; //Data atau isian yang akan dimanipulasi, untuk genre_id harus siapkan model dari genre agar datanya bisa dibaca oleh Film.php (use App\Models\Genre)
}
