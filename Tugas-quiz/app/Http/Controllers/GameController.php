<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function create() {
        return view ('games.createGame');
    }

    public function store(Request $request) { //Function submitdata ke database
        //Validasi
        $request->validate([
            'name' => 'required|min:6',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        //Memasukkan ke database
        DB::table('game')->insert([
            'name' => $request->input('name'),
            'gameplay' => $request->input('gameplay'),
            'developer' => $request->input('developer'),
            'year' => $request->input('year')
        ]);

        return redirect('/games');
    }

    public function index() {
        $game = DB::table('game')->get();

        return view('games.indexGame', ['game'=>$game]);
    }

    public function show($id) {
        $games = DB::table('game')->find($id);

        return view('games.showGame', ['games' => $games]);
    }

    public function edit($id) {
        $games = DB::table('game')->find($id);

        return view('games.editGame', ['games' => $games]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'required|min:6',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);
        
        DB::table('game')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request->input('name'),
                    'gameplay' => $request->input('gameplay'),
                    'developer' => $request->input('developer'),
                    'year' => $request->input('year')
                ]
            );
        return redirect('/games');
    }

    public function destroy($id){
        DB::table('game')->where('id', '=', $id)->delete();

        return redirect('/games');
    }
}
