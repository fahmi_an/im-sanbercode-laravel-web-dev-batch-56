<?php

use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;
use PHPUnit\Framework\Attributes\RunTestsInSeparateProcesses;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// CRUD
// Mengarahkan ke form tambah Game
Route::get('/', [GameController::class, 'create']);
// Menyimpan data yang diinput dari form tambahGame
Route::post('/games', [GameController::class, 'store']);

// Menampilkan Tabel Game
Route::get('/games', [GameController::class, 'index']);
// Tombol Detail
Route::get('/games/{id}', [GameController::class, 'show']);

//form edit
Route::get('/games/{id}/edit', [GameController::class, 'edit']);
//update data
Route::put('/games/{id}', [GameController::class, 'update']);

// Hapus
Route::delete('/games/{id}', [GameController::class, 'destroy']);
