@extends('layouts.master')

@section('title')
Profile User
@endsection

@section('content')
<form method="POST" action="/profile/{{$profile->id}}">
  <!-- method digunakan untuk mengganti method POST pada form diatas menjadi put -->
  @method('put')
  <!-- ini merupakan codingan validasi yang didapat dari dokumentasi laravelnya -->
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <!-- Form Input -->
  @csrf
  <!-- <p>Nama User : {{ $profile->user->name }}</p>
  <p>Email User : {{ $profile->user->email }}</p> -->
  
  <div class="form-group">
    <label>Usia User</label>
    <input type="number" value="{{ $profile->umur }}" name="umur" class="form-control">
  </div>
  <div class="form-group">
    <label>Biodata User</label>
    <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{ $profile->bio }}</textarea>
  </div>
  <div class="form-group">
    <label>Alamat User</label>
    <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control">{{ $profile->alamat }}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection