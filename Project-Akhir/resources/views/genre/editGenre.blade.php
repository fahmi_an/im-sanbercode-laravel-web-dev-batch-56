@extends('layouts.master')

@section('title')
Edit Genre
@endsection

@section('content')
<form method="POST" action="/genre/{{$genre->id}}">
  <!-- method digunakan untuk mengganti method POST pada form diatas menjadi put -->
  @method('put')
  <!-- ini merupakan codingan validasi yang didapat dari dokumentasi laravelnya -->
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <!-- Form Input -->
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" value="{{$genre->nama}}" name="nama" class="form-control">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection