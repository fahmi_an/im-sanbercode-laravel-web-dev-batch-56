@extends('layouts.master')

@section('title')
Tambah Genre
@endsection

@section('content')
<form method="post" action="/genre">
  <!-- ini merupakan codingan validasi yang didapat dari dokumentasi laravelnya -->
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <!-- Form Input -->
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection