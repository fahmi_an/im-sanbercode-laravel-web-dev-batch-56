@extends('layouts.master')

@section('title')
Daftar Genre Film
@endsection

@section('content')
<a href="/genre/create" class="btn btn-sm btn-primary">Tambah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Genre</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genre as $key => $item)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->nama}}</td>
        <td>
          <form action="/genre/{{$item->id}}" method="POST">
            @csrf
            @method('delete')
            <a href="/genre/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
            <a href="/genre/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" class="btn btn-sm btn-danger" value="Delete">
          </form>
        </td>
      </tr>
    @empty
      <p>No Genre</p>
    @endforelse
  </tbody>
</table>
@endsection