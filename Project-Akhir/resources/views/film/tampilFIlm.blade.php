@extends('layouts.master')

@section('title')
Tambah Film
@endsection

@section('content')
<a href="/film/create" class="btn btn-sm btn-primary my-3">Tambah</a>

<div class="row">
    @forelse ($film as $item)

     <div class="col-4">
        <div class="card">
            <img src="{{asset('poster/'.$item->poster)}}" class="card-img-top" width="200px" height="200px" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{$item->judul}}</h5>
                <p class="card-text">{{$item->tahun}}</p>
                <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
                <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
                
                @auth
                    <div class="row my-3">
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                        </div>
                        <div class="col"> 
                            <form action="/film/{{$item->id}}" method="POST">
                                @csrf
                                @method('delete')

                                <input type="submit" class="btn btn-danger btn-block" value="Delete">
                            </form>
                        </div>
                    </div>
                @endauth
                
            </div>
        </div>
     </div>
        
    @empty
        
    @endforelse
</div>
@endsection