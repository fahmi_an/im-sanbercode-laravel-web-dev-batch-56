@extends('layouts.master')

@section('title')
Tambah Film
@endsection

@section('content')
<form method="post" action="/film" enctype="multipart/form-data"> <!-- enctype digunakan untuk menampung nilai dari inputan -->
  <!-- ini merupakan codingan validasi yang didapat dari dokumentasi laravelnya -->
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif

  <!-- Form Input -->
  @csrf
  <div class="form-group">
    <label>Judul Film</label>
    <input type="text" name="judul" class="form-control">
  </div>
  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
  </div>
  <div class="form-group">
    <label>Tahun</label>
    <input type="text" name="tahun" class="form-control">
  </div>
  <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" id="" class="form-control">
        <option value="">--Pilih Genre--</option>
        @forelse ($genre as $item)
        <option value="{{$item->id}}">{{$item->nama}}</option>
        @empty
        <option value="">{{$item->nama}}</option>
        @endforelse
    </select>
  </div>
  <div class="form-group">
    <label>Poster</label>
    <input type="file" name="poster" class="form-control">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection