<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('syuting', function (Blueprint $table) {
            $table->id();
            $table->string("nama_peran");
            $table->unsignedBigInteger("film_id");
            $table->unsignedBigInteger("peran_id");
                        
            $table->foreign("film_id")->references("id")->on("film");
            $table->foreign("peran_id")->references("id")->on("peran");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('syuting');
    }
};
