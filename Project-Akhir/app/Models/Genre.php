<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $table = "genre"; //Mengarahkan ke database film kita

    protected $fillable = ['id', 'nama'];

    public function listFilm () {
        return $this->hasMany(Film::class, 'genre_id');
    }
}
