<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    use HasFactory;
    protected $table = "peran"; //Mengarahkan ke database film kita

    protected $fillable = ['id', 'name', 'umur', 'bio']; //Data atau isian yang akan dimanipulasi
}
