<?php

namespace App\Http\Controllers;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index() {
        $idUser = Auth::id();

        $profile = Profile::where('users_id', $idUser)->first();

        return view('profile.tampilProfile', ['profile' => $profile]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        $profile = Profile::find($id);

        $profile->umur = $request->input('umur');
        $profile->bio = $request->input('bio');
        $profile->alamat = $request->input('alamat');
 
        $profile->save();

        return redirect('/profile');
    }
}
