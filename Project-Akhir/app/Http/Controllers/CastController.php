<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function mainMenu() { //function yang dipanggil di coding web.php
        return view('index'); //view('..') akan menampilkan halaman welcome
    }
    
    public function create() {
        return view ('cast.tambahCast');
    }

    public function store(Request $request) { //Function submit data ke database
        //Validasi
        $request->validate([
            'name' => 'required|min:6',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        //Memasukkan ke database
        DB::table('peran')->insert([
            'name' => $request->input('name'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        return redirect('/cast');
    }

    public function index() {
        $peran = DB::table('peran')->get();

        return view('cast.tampilCast', ['peran'=>$peran]);
    }

    public function show($id) {
        $cast = DB::table('peran')->find($id);

        return view('cast.showCast', ['cast' => $cast]);
    }

    public function edit($id) {
        $cast = DB::table('peran')->find($id);

        return view('cast.editCast', ['cast' => $cast]);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'name' => 'required|min:6',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        
        DB::table('peran')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request->input('name'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]
            );
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('peran')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
