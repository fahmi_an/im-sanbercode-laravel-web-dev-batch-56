<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;

class GenreController extends Controller
{

     /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $genre = Genre::get();

        return view ('genre.tampilGenre', ['genre' => $genre]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('genre.tambahGenre');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);
      

        $genre = new Genre;
 
        $genre->nama = $request->input('nama');
 
        $genre->save();
 
        return redirect('/genre');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $genre = Genre::find($id);

        return view('genre.detailGenre', ['genre' => $genre]); // Jika menggunakan redirect, maka akan error
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $genre = Genre::find($id);

        return view('genre.editGenre', ['genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);
      

        $genre = Genre::find($id);
 
        $genre->nama = $request->input('nama');
 
        $genre->save();
 
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $genre = Genre::find($id);
        
        $genre->delete();

        return redirect('/genre');
    }
}
